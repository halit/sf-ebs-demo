const extend = require("js-base/core/extend");
const Color = require("sf-core/ui/color");
const Label = require("sf-core/ui/label");

const COLOR_INACTIVE = Color.create("#D8D8D8");
const COLOR_ACTIVE = Color.create("#1775D0");

var DotView = extend(Label)(
    function(_super, params) {
        _super(this, params);
        
        this.height = 10;
        this.width = 10;
        this.borderRadius = 5;
        this.backgroundColor = COLOR_INACTIVE;
        this.active = false;
        this.marginLeft = 2;
        this.marginRight = 2;
        
        this.activate = function() {
            if (!this.active) {
                this.backgroundColor = COLOR_ACTIVE;
                this.active = true;
            }
        }
        
        this.deactivate = function() {
            if (this.active) {
                this.backgroundColor = COLOR_INACTIVE;
                this.active = false;
            }
        }
    }
);

module.exports = DotView;