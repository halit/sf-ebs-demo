/* 
		WARNING 
		Auto generated file. 
		Do not modify its contents.
*/

const extend = require('js-base/core/extend');
const Page = require('sf-core/ui/page');
const FlexLayout = require('sf-core/ui/flexlayout');
const Color = require('sf-core/ui/color');
const Label = require('sf-core/ui/label');
const TextAlignment = require('sf-core/ui/textalignment');
const Font = require('sf-core/ui/font');
const StatusBarStyle = require('sf-core/ui/statusbarstyle');



const PgProfile_ = extend(Page)(
	//constructor
	function(_super){
		// initalizes super class for this page scope
		_super(this, {
			onLoad: onLoad.bind(this),
			orientation: Page.Orientation.PORTRAIT
		});

		var swipeLayout = new FlexLayout({
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 4,
			alignSelf: FlexLayout.AlignSelf.STRETCH,
			backgroundColor: Color.create(255, 69, 73, 90),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			visible: true
		}); 
		this.layout.addChild(swipeLayout);
		this.swipeLayout = swipeLayout;
		var titleLayout = new FlexLayout({
			height: 44,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			visible: true
		}); 
		swipeLayout.addChild(titleLayout);
		this.titleLayout = titleLayout;
		var dotsLayout = new FlexLayout({
			height: 20,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.CENTER,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			visible: true
		}); 
		swipeLayout.addChild(dotsLayout);
		this.dotsLayout = dotsLayout;
		var title = new Label({
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1,
			alignSelf: FlexLayout.AlignSelf.STRETCH,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 218, 219, 222),
			textAlignment: TextAlignment.MIDCENTER,
			visible: true,
			text: "OVERVIEW"
		});
		title.font = Font.create("Arial", 16, Font.NORMAL); 
		titleLayout.addChild(title);
		this.title = title;
		
		//assign the children to page 
		this.children = Object.assign({}, {
			swipeLayout: swipeLayout
		});
		
		//assign the children of swipeLayout
		swipeLayout.children =  Object.assign({}, {
			titleLayout: titleLayout,
			dotsLayout: dotsLayout
		});
		
		//assign the children of titleLayout
		titleLayout.children =  Object.assign({}, {
			title: title
		});

});

function onLoad() { 

  this.headerBar.title = "OVERVIEW";
  this.headerBar.titleColor = Color.create(255, 218, 219, 222);
  this.headerBar.backgroundColor = Color.create(255, 69, 73, 90);
  this.headerBar.visible = false;
  this.statusBar.visible = true;this.statusBar.android && (this.statusBar.android.color = Color.create(255, 69, 73, 90));this.statusBar.ios && (this.statusBar.ios.style = StatusBarStyle.LIGHTCONTENT);
  this.layout.flexDirection = FlexLayout.FlexDirection.COLUMN;
  this.layout.alignItems = FlexLayout.AlignItems.CENTER;
  this.layout.direction = FlexLayout.Direction.INHERIT;
  this.layout.flexWrap = FlexLayout.FlexWrap.NOWRAP;
  this.layout.justifyContent = FlexLayout.JustifyContent.SPACE_AROUND;
  this.layout.backgroundColor = Color.create("#EEEEEE");

}

module && (module.exports = PgProfile_);