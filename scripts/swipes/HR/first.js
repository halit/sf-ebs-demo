const extend = require("js-base/core/extend");
const Color = require("sf-core/ui/color");
const FlexLayout = require("sf-core/ui/flexlayout");
const Image = require("sf-core/ui/image");
const ImageView = require("sf-core/ui/imageview");
const Page = require("sf-core/ui/page");

var Page1 = extend(Page)(
    function(_super, params) {
        _super(this, params);
        
        this.onLoad = function() {
            this.layout.backgroundColor = Color.create("#45495A");
            this.layout.justifyContent = FlexLayout.JustifyContent.CENTER;
            this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
            
            var topLayout = new FlexLayout({
                flexGrow: 5,
                justifyContent: FlexLayout.JustifyContent.CENTER,
                alignItems: FlexLayout.AlignItems.CENTER,
                flexDirection: FlexLayout.FlexDirection.ROW
            });
            this.layout.addChild(topLayout);
            
            var bottomLayout = new FlexLayout({
                flexGrow: 8,
                backgroundColor: Color.create("#EAEAEB"),
            });
            this.layout.addChild(bottomLayout);
            
            var empty = new ImageView({
                flexGrow: 1
            });
            topLayout.addChild(empty);
            
            var rounded = new ImageView({
                flexGrow: 1,
                image: Image.createFromFile("images://rounded.png"),
                imageFillType: ImageView.FillType.ASPECTFIT
            });
            topLayout.addChild(rounded);
            
            var colors = new ImageView({
                flexGrow: 1,
                marginTop: 10,
                marginLeft: 10,
                image: Image.createFromFile("images://colors.png"),
                imageFillType: ImageView.FillType.ASPECTFIT
            });
            topLayout.addChild(colors);
            
        }.bind(this);
    }
);

module.exports = Page1;