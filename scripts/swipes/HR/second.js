const extend = require("js-base/core/extend");
const Color = require("sf-core/ui/color");
const FlexLayout = require("sf-core/ui/flexlayout");
const Image = require("sf-core/ui/image");
const ImageView = require("sf-core/ui/imageview");
const Label = require("sf-core/ui/label");
const Page = require("sf-core/ui/page");

var Page1 = extend(Page)(
    function(_super, params) {
        _super(this, params);
        
        this.onLoad = function() {
            this.layout.backgroundColor = Color.create("#45495A");
            this.layout.justifyContent = FlexLayout.JustifyContent.CENTER;
            this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
            
            var topLayout = new FlexLayout({
                flexGrow: 2,
                justifyContent: FlexLayout.JustifyContent.CENTER,
                alignItems: FlexLayout.AlignItems.CENTER
            });
            this.layout.addChild(topLayout);
            
            var bottomLayout = new FlexLayout({
                flexGrow: 8,
                backgroundColor: Color.create("#EAEAEB"),
            });
            this.layout.addChild(bottomLayout);
            
            var profileImage = new ImageView({
                height: 96,
                width: 300,
                image: Image.createFromFile("images://month_picker.png"),
                imageFillType: ImageView.FillType.ASPECTFIT
            });
            topLayout.addChild(profileImage);
            
        }.bind(this);
    }
);

module.exports = Page1;