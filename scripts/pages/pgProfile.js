const extend = require("js-base/core/extend");
const DotView = require("lib/DotView");
const FlexLayout = require("sf-core/ui/flexlayout");
const SwipeView  = require('sf-core/ui/swipeview');
const System = require("sf-core/device/system");

// Get generetad UI code
var Page2Design = require("../ui/ui_pgProfile");

const Page2 = extend(Page2Design)(
    function(_super) {
        _super(this);
        initSwipeView(this);

        this.onShow = onShow.bind(this);
    }
);

function onShow() {
    initHeaderBar(this.headerBar);
    if (System.OS === "iOS") {
        this.titleLayout.marginTop = 20;
    }
}

function initHeaderBar(headerBar) {
    headerBar.leftItemEnabled = false;
}

function initSwipeView(page) {
    var dotViews = [new DotView(), new DotView(), new DotView()];
    dotViews.forEach(function(dotView) {
        page.dotsLayout.addChild(dotView);
    });
    
    var titles = ["OVERVIEW", "PERFORMANCE", "SALARY"];

    var swipeView = new SwipeView({
        page: page,
        flexGrow: 1,
        alignSelf: FlexLayout.AlignSelf.STRETCH,
        pages: [
                require("swipes/profile/first"),
                require("swipes/profile/second"),
                require("swipes/profile/third")
            ],
        onStateChanged: function(state) {},
        onPageSelected: function(index) {
            dotViews.forEach(function(dotView) {
                dotView.deactivate();
            });
            dotViews[index].activate();
            page.title.text = titles[index];
        }
    });
    page.swipeLayout.addChild(swipeView);
    dotViews[0].activate();
}

module && (module.exports = Page2);
